package taller.estructuras;


public class TablaHash<K extends Comparable<K> ,V> {

	//TODO Una enumeracioón que representa los tipos de colisiones que se pueden manejar

	/**
	 * Factor de carga actual de la tabla (porcentaje utilizado).
	 */
	private float factorCarga;

	/**
	 * Factor de carga maximo que soporta la tabla.
	 */
	private float factorCargaMax;
	
	/**
	 * Estructura que soporta la tabla.
	 */
	//TODO: Agruegue la estructura que va a soportar la tabla.
	private Object[] arreglo;

	/**
	 * La cuenta de elementos actuales.
	 */
	private int count;

	/**
	 * La capacidad actual de la tabla. Tamaño del arreglo fijo.
	 */
	private int capacidad;

	//Constructores

	@SuppressWarnings("unchecked")
	public TablaHash(){
		//TODO: Inicialice la tabla con los valores que considere prudentes para una ejecución normal
		this(4 , (float) 0.9 );
	}

	@SuppressWarnings("unchecked")
	public TablaHash(int xcapacidad, float xfactorCargaMax) {
		//TODO: Inicialice la tabla con los valores dados por parametro
		capacidad = xcapacidad;
		factorCargaMax = xfactorCargaMax;
		count = 0;
		factorCarga = count/capacidad;
		arreglo = new Object[capacidad];
	}

	public void put(K llave, V valor){
		//TODO: Gaurde el objeto valor dado por parametro el cual tiene la llave,
		//tenga en cuenta que puede o no puede haber colisiones
		int pos = hash(llave);
		NodoHash<K, V> nuevo = new NodoHash<K, V>(llave, valor);
		if(arreglo[pos] == null)
		{
			arreglo[pos] = nuevo;
			count ++;
		}
		else
		{
			boolean agrego = false;
			NodoHash<K, V> actual = ((NodoHash<K, V>) arreglo[pos]);
			while(!agrego)
			{	 
				if(actual.darSiguiente() == null)
				{
					actual.cambiarSiguiente(nuevo);
					agrego = true;
					count ++;
				}
				else
					actual = actual.darSiguiente();
			}
		}

	}

	public V get(K llave){
		//TODO: Busque y retorne el objeto cuya llave es la dada por parametro. Tenga en cuenta
		// colisiones
		int pos = hash(llave);
		NodoHash<K, V> actual = (NodoHash<K, V>) arreglo[pos];
		boolean encontro = false;
		while(!encontro)
		{
			if( actual.getLlave().equals(llave))
			{
				encontro = true;
				return actual.getValor();
			}
			else
				actual = actual.darSiguiente();
		}
		return null;
	}

	public V delete(K llave){
		//TODO: borra el objeto cuya llave es la dada por parametro. Tenga en cuenta
		// colisiones
		int pos = hash(llave);
		V resp = null;
		if(((NodoHash<K, V>) arreglo[pos]).getLlave().equals(llave))
		{
			resp = ((NodoHash<K, V>) arreglo[pos]).getValor();
			arreglo[pos] = ((NodoHash<K, V>) arreglo[pos]).darSiguiente();
			count --;
		}
		else
		{	
			NodoHash<K, V> actual = (NodoHash<K, V>) arreglo[pos];
			boolean eliminado = false;
			while(!eliminado)
			{
				if(actual.darSiguiente().getLlave().equals(llave))
				{
					resp = actual.darSiguiente().getValor();
					actual.cambiarSiguiente(actual.darSiguiente().darSiguiente());
				}
				else
				{
					actual = actual.darSiguiente();
				}
			}
		}
		return resp;
	}

	//Hash
	private int hash(K llave)
	{
		//TODO: Escriba una función de Hash, recuerde tener en cuenta la complejidad de ésta así como las colisiones.
		return llave.hashCode() & capacidad;
	}
	
	public resize()
	{
		capacidad = capacidad*2;
		TablaHash nueva = new TablaHash(capacidad*2, (float) 0.9);
		for(int i = 0; i < capacidad ; i++)
		{
			NodoHash<K, V> actual = (NodoHash<K, V>) arreglo[i];
			if(arreglo[i] != null)
				nueva.put(actual.getLlave(), actual.getValor()); 
		}
		arreglo = nueva.arreglo;
		
	}
		//TODO: Permita que la tabla sea dinamica

}